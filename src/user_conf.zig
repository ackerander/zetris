const std = @import("std");
const config = @import("config");
const c = @import("c_includes.zig");

pub const Settings = struct {
    key_binds: struct {
        directions: [2]c_int = .{c.KEY_LEFT, c.KEY_RIGHT},
        rotations: [2]c_int = .{c.KEY_Z, c.KEY_X},
        drops: [2]c_int = .{c.KEY_SPACE, c.KEY_DOWN},
        hold: c_int = c.KEY_C,
        quit: c_int = c.KEY_Q,
    },
    resolution: ?@Vector(2, c_int) = null,
};

pub fn loadConf() !Settings {
    const alloc = std.heap.c_allocator;

    // const test_settings = Settings {
    //     .key_binds = .{},
    //     .resolution = .{1080, 1080},
    // };
    // var json = std.ArrayList(u8).init(alloc);
    // defer json.deinit();
    // try std.json.stringify(test_settings, .{}, json.writer());
    // std.debug.print("\n{s}\n", .{json.items});

    const file = try std.fs.cwd().openFile("settings.json", .{});
    defer file.close();

    var json_reader = std.json.reader(alloc, file.reader());
    defer json_reader.deinit();

    const parsed = try std.json.parseFromTokenSource(Settings, alloc, &json_reader, .{});
    defer parsed.deinit();
    std.debug.print("{any}\n", .{parsed.value});

    return parsed.value;
}
