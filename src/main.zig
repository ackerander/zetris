const std = @import("std");
const config = @import("config");
const user_conf = @import("user_conf.zig");
const c = @import("c_includes.zig");

const Dir = enum(u2) { n = 0, e = 1, s = 2, w = 3,
    pub fn rotate(self: @This(), center: @Vector(2, u5), pos: @Vector(2, u5)) @Vector(2, i6) {
        return switch (self) {
            .n => pos,
            .e => .{@as(i6, pos[1]) - center[1] + center[0], @as(i6, center[0]) - pos[0] + center[1]},
            .s => .{@as(i6, center[0]) - pos[0] + center[0], @as(i6, center[1]) - pos[1] + center[1]},
            .w => .{@as(i6, center[0]) - pos[1] + center[1], @as(i6, pos[0]) - center[0] + center[1]},
        };
    }
    pub fn rotateI(self: @This(), center: @Vector(2, i6), pos: @Vector(2, u5)) @Vector(2, i6) {
        return switch (self) {
            .n => pos,
            .e => .{pos[1] - center[1] + center[0], center[0] - pos[0] + center[1] + 1},
            .s => .{center[0] - pos[0] + center[0] + 1, center[1] - pos[1] + center[1] + 1},
            .w => .{center[1] - pos[1] + center[0] + 1, pos[0] - center[0] + center[1]},
        };
    }
    pub inline fn add(self: @This(), other: @This()) @This() {
        return @enumFromInt(@intFromEnum(self) +% @intFromEnum(other));
    }
};
const Mino = enum {
    i, o, t, s, z, j, l,
    pub fn toTile(mino: @This()) Tile { return @enumFromInt(@intFromEnum(mino) + 1); }
    pub fn drawPreview(mino: @This(), x: c_int, y: c_int) void {
        switch (mino) {
            .i => c.DrawRectangle(x, y, 4 * screen_coords.tile_size, screen_coords.tile_size, c.SKYBLUE),
            .o => c.DrawRectangle(x + screen_coords.tile_size, y, 2 * screen_coords.tile_size, 2 * screen_coords.tile_size, c.YELLOW),
            .t => {
                c.DrawRectangle(x + screen_coords.tile_size, y, screen_coords.tile_size, screen_coords.tile_size, c.PURPLE);
                c.DrawRectangle(x, y + screen_coords.tile_size, 3 * screen_coords.tile_size, screen_coords.tile_size, c.PURPLE);
            },
            .s => {
                c.DrawRectangle(x + screen_coords.tile_size, y, 2 * screen_coords.tile_size, screen_coords.tile_size, c.GREEN);
                c.DrawRectangle(x, y + screen_coords.tile_size, 2 * screen_coords.tile_size, screen_coords.tile_size, c.GREEN);
            },
            .z => {
                c.DrawRectangle(x, y, 2 * screen_coords.tile_size, screen_coords.tile_size, c.RED);
                c.DrawRectangle(x + screen_coords.tile_size, y + screen_coords.tile_size, 2 * screen_coords.tile_size, screen_coords.tile_size, c.RED);
            },
            .j => {
                c.DrawRectangle(x, y, screen_coords.tile_size, screen_coords.tile_size, c.BLUE);
                c.DrawRectangle(x, y + screen_coords.tile_size, 3 * screen_coords.tile_size, screen_coords.tile_size, c.BLUE);
            },
            .l => {
                c.DrawRectangle(x + 2 * screen_coords.tile_size, y, screen_coords.tile_size, screen_coords.tile_size, c.ORANGE);
                c.DrawRectangle(x, y + screen_coords.tile_size, 3 * screen_coords.tile_size, screen_coords.tile_size, c.ORANGE);
            },
        }
    }
    pub fn getSquares(self: @This()) [4]@Vector(2, u2) {
        return switch (self) {
            .i => .{.{1, 1}, .{2, 1}, .{0, 1}, .{3, 1}},
            .o => .{.{1, 0}, .{1, 1}, .{2, 0}, .{2, 1}},
            .t => .{.{1, 0}, .{0, 0}, .{1, 1}, .{2, 0}},
            .s => .{.{1, 0}, .{0, 0}, .{1, 1}, .{2, 1}},
            .z => .{.{1, 0}, .{0, 1}, .{1, 1}, .{2, 0}},
            .j => .{.{1, 0}, .{0, 0}, .{0, 1}, .{2, 0}},
            .l => .{.{1, 0}, .{0, 0}, .{2, 0}, .{2, 1}},
        };
    }
    pub fn offsetSquares(self: @This(), offset: @Vector(2, u5)) [4]@Vector(2, u5) {
        var out: [4]@Vector(2, u5) = undefined;
        for (self.getSquares(), &out) |sq, *o|
            o.* = sq + offset;
        return out;
    }
};
const Tile = enum(u4) {
    empty = 0,
    i, o, t, s, z, j, l, junk,
    pub fn color(self: @This(), comptime transparent: bool) c.Color {
        var base_color = switch (self) {
            .empty => c.BLANK,
            .i => c.SKYBLUE,
            .o => c.YELLOW,
            .t => c.PURPLE,
            .s => c.GREEN,
            .z => c.RED,
            .j => c.BLUE,
            .l => c.ORANGE,
            .junk => c.LIGHTGRAY,
        };
        if (transparent)
            base_color.a = 0x67;
        return base_color;
    }
};

inline fn drawSquare(pos: @Vector(2, u5), color: c.Color) void {
    c.DrawRectangle(screen_coords.tile_size * @as(c_int, pos[0]) + screen_coords.start[0], screen_coords.end[1] - screen_coords.tile_size * @as(c_int, pos[1] + 1), screen_coords.tile_size, screen_coords.tile_size, color);
}
fn vec2Cast(T: type, v: anytype) ?@Vector(2, T) {
    var out: @Vector(2, T) = undefined;
    inline for (0..2) |i|
        out[i] = std.math.cast(T, v[i]) orelse return null;
    return out;
}
fn fullCell(vec: anytype) bool {
    const pos = vec2Cast(u5, vec) orelse return true;
    return pos[0] >= BOARD_SZ.cols or pos[1] < BOARD_SZ.rows and board[pos[1]][pos[0]] != .empty;
}

const Piece = struct {
    const LOCKTIME = 0.5;
    squares: [4]@Vector(2, u5),
    kind: Mino,
    drop_dist: u5,
    rotation: Dir = .n,
    lock_timer: f32 = 0,
    resets: u4 = 0,
    pub fn draw(self: @This()) void {
        inline for (self.squares) |sq| {
            if (sq[1] < BOARD_SZ.rows)
                drawSquare(sq, self.kind.toTile().color(false));
        }
        if (self.drop_dist > 0) {
            inline for (self.squares) |sq|
                drawSquare(sq - @Vector(2, u5){0, self.drop_dist}, self.kind.toTile().color(true));
        }
    }
    fn calcDrop(self: *@This()) u5 {
        self.drop_dist = 1;
        while (!self.collision(.{0, -@as(i6, self.drop_dist)})) : (self.drop_dist += 1) {}
        self.drop_dist -= 1;
        return self.drop_dist;
    }
    fn collision(self: @This(), delta: @Vector(2, i6)) bool {
        inline for (self.squares) |sq|
            if (fullCell(sq + delta)) return true;
        return false;
    }
    fn move_reset(self: *@This()) bool {
        if (self.resets < 15 and self.drop_dist == 0) {
            self.lock_timer = 0;
            self.resets += 1;
            return true;
        } else {
            return false;
        }
    }
    pub fn updateLockT(self: *@This(), dt: f32) void {
        if (self.drop_dist == 0) {
            self.lock_timer += dt;
            if (self.lock_timer >= LOCKTIME)
                self.drop();
        }
    }
    pub fn move(self: *@This(), dir: Dir) bool {
        const delta: @Vector(2, i6) = switch (dir) {
            .n => return false,
            .e => .{1, 0},
            .s => .{0, -1},
            .w => .{-1, 0},
        };
        if (self.collision(delta)) return false;
        for (&self.squares) |*square|
            square.* = vec2Cast(u5, square.* + delta) orelse unreachable;
        _ = self.move_reset();
        _ = self.calcDrop();
        return true;
    }
    pub fn spawn(self: *@This(), comptime swap: bool) void {
        self.kind = if (swap) blk: {
            const current_hold = hold;
            hold = self.kind;
            swaped = true;
            break :blk if (current_hold) |mino| mino else queue.next();
        } else queue.next();
        self.squares = self.kind.offsetSquares(.{3, if (self.kind == .i) 18 else 19});
        for (self.squares) |sq| {
            if (fullCell(sq)) game_state = .game_over;
        }
        self.rotation = .n;
        _ = self.calcDrop();
        self.lock_timer = 0;
        self.resets = 0;
    }
    pub fn drop(self: *@This()) void {
        var to_clear = std.StaticBitSet(BOARD_SZ.rows).initEmpty();
        var topout = false;
        for (self.squares) |sq| {
            const row = sq[1] - self.drop_dist;
            if (row >= BOARD_SZ.rows) {
                topout = true;
                continue;
            }
            board[row][sq[0]] = self.kind.toTile();
            to_clear.setValue(row,
                for (board[row]) |t| {
                    if (t == .empty) break false;
                } else true
            );
        }
        // clear lines
        var itr = to_clear.iterator(.{});
        if (itr.next()) |first| {
            var offset: usize = 1;
            var last = first + 1;
            while (itr.next()) |i| {
                if (i - last > 0)
                    std.mem.copyForwards([BOARD_SZ.cols]Tile, board[last - offset..], board[last..i]);
                last = i + 1;
                offset += 1;
            }
            std.mem.copyForwards([BOARD_SZ.cols]Tile, board[last - offset..], board[last..]);
            @memset(board[BOARD_SZ.rows - offset..], .{.empty} ** 10);
        }

        if (topout) {
            game_state = .game_over;
        } else {
            self.spawn(false);
        }
        swaped = false;
    }
    pub fn rotate(self: *@This(), dir: Dir) bool {
        const table_row = @intFromEnum(self.rotation) + @as(u3, switch (dir) {
            .e => 0,
            .w => 4,
            else => return false,
        });
        switch (self.kind) {
            .i => {
                const center: @Vector(2, i6) = switch (self.rotation) {
                    .n => self.squares[0] - @Vector(2, i6){0, 1},
                    .e => self.squares[0] - @Vector(2, i6){1, 1},
                    .s => self.squares[0] - @Vector(2, i6){1, 0},
                    .w => self.squares[0],
                };
                const kick_table = [8][5]@Vector(2, i3){
                    .{.{0, 0}, .{-2, 0}, .{ 1, 0}, .{-2, -1}, .{ 1,  2}}, // n + e
                    .{.{0, 0}, .{-1, 0}, .{ 2, 0}, .{-1,  2}, .{ 2, -1}}, // e + e
                    .{.{0, 0}, .{ 2, 0}, .{-1, 0}, .{ 2,  1}, .{-1, -2}}, // s + e
                    .{.{0, 0}, .{ 1, 0}, .{-2, 0}, .{ 1, -2}, .{-2,  1}}, // w + e
                    .{.{0, 0}, .{-1, 0}, .{ 2, 0}, .{-1,  2}, .{ 2, -1}}, // n + w
                    .{.{0, 0}, .{ 2, 0}, .{-1, 0}, .{ 2,  1}, .{-1, -2}}, // e + w
                    .{.{0, 0}, .{ 1, 0}, .{-2, 0}, .{ 1, -2}, .{-2,  1}}, // s + w
                    .{.{0, 0}, .{-2, 0}, .{ 1, 0}, .{-2, -1}, .{ 1,  2}}, // w + w
                };
                const kick = outer: for (kick_table[table_row]) |k| {
                    inline for (self.squares) |sq|
                        if (fullCell(dir.rotateI(center, sq) + k)) continue :outer;
                    break k;
                } else return false;
                inline for (&self.squares) |*sq|
                    sq.* = @intCast(dir.rotateI(center, sq.*) + kick);
            },
            .o => {},
            else => {
                const kick_table = [8][5]@Vector(2, i3){
                    .{.{0, 0}, .{-1, 0}, .{-1,  1}, .{0, -2}, .{-1, -2}}, // n + e
                    .{.{0, 0}, .{ 1, 0}, .{ 1, -1}, .{0,  2}, .{ 1,  2}}, // e + e
                    .{.{0, 0}, .{ 1, 0}, .{ 1,  1}, .{0, -2}, .{ 1, -2}}, // s + e
                    .{.{0, 0}, .{-1, 0}, .{-1, -1}, .{0,  2}, .{-1,  2}}, // w + e
                    .{.{0, 0}, .{ 1, 0}, .{ 1,  1}, .{0, -2}, .{ 1, -2}}, // n + w
                    .{.{0, 0}, .{ 1, 0}, .{ 1, -1}, .{0,  2}, .{ 1,  2}}, // e + w
                    .{.{0, 0}, .{-1, 0}, .{-1,  1}, .{0, -2}, .{-1, -2}}, // s + w
                    .{.{0, 0}, .{-1, 0}, .{-1, -1}, .{0,  2}, .{-1,  2}}, // w + w
                };
                const kick = outer: for (kick_table[table_row]) |k| {
                    inline for (self.squares[1..]) |sq|
                        if (fullCell(dir.rotate(self.squares[0], sq) + k)) continue :outer;
                    if (fullCell(@as(@Vector(2, i6), self.squares[0]) + k)) continue;
                    break k;
                } else return false;
                inline for (self.squares[1..]) |*sq|
                    sq.* = @intCast(dir.rotate(self.squares[0], sq.*) + kick);
                self.squares[0] = @intCast(@as(@Vector(2, i6), self.squares[0]) + kick);
            },
        }
        self.rotation = self.rotation.add(dir);
        _ = self.move_reset();
        _ = self.calcDrop();
        return true;
    }
};

fn MinoQueue(cap: comptime_int) type {
    return struct {
        data: [cap]Mino,
        head: u8 = 0,
        tail: u8 = 0,
        len: u8 = 0,
        fn maybe_pop(self: @This()) ?Mino { return if (self.len != 0) self.pop() else null; }
        fn pop(self: *@This()) Mino {
            const old_head = self.head;
            self.head = if (self.head + 1 >= cap) 0 else self.head + 1;
            self.len -= 1;
            return self.data[old_head];
        }
        fn newBag(self: *@This()) void {
            if (self.len + 7 > cap) return;
            var bag: [7]Mino = undefined;
            @memcpy(&bag, std.enums.values(Mino));
            rng.shuffle(Mino, &bag);
            if (self.tail + 7 > cap) {
                const break_idx = cap - self.tail;
                const end_idx = 7 - break_idx;
                @memcpy(self.data[self.tail..], bag[0..break_idx]);
                @memcpy(self.data[0..end_idx], bag[break_idx..]);
                self.tail = end_idx;
            } else {
                @memcpy(self.data[self.tail..self.tail + 7], &bag);
                self.tail += 7;
            }
            self.len += 7;
        }
        pub fn getUnchecked(self: @This(), i: u8) Mino {
            const idx = self.head + i;
            return self.data[if (idx >= cap) i - (cap - self.head) else idx];
        }
        pub fn get(self: @This(), i: u8) ?Mino {
            return if (i >= self.len) null else self.getUnchecked(i);
        }
        pub fn next(self: *@This()) Mino {
            self.newBag();
            const result = self.pop();
            return result;
        }
    };
}

const DasState = struct {
    const DASTIME = 0.167;
    const REPTIME = 0.033;
    time: f32,
    dir: Dir,
    state: enum { idle, das, repeat },
    pub fn input(self: *@This(), in_mask: u2, time: f32) void {
        const dir: Dir = switch (in_mask) {
            0b01 => .e,
            0b10 => .w,
            else => {
                self.time = 0;
                if (self.state == .das)
                    _ = current_piece.move(self.dir);
                self.state = .idle;
                return;
            },
        };
        self.time += time;
        const timer: f32 = switch (self.state) {
            .idle => {
                self.dir = dir;
                self.state = .das;
                return;
            },
            .das => DASTIME,
            .repeat => REPTIME,
        };
        if (self.dir != dir)
            self.* = .{ .time = time, .dir = dir, .state = .das };
        if (self.time >= timer) {
            self.time -= timer;
            self.state = .repeat;
            _ = current_piece.move(dir);
        }
    }
};

const ScreenCoords = struct {
    dims: @Vector(2, c_int),
    start: @Vector(2, c_int),
    board_span: @Vector(2, c_int),
    end: @Vector(2, c_int),
    tile_size: c_int,
    half_tile: c_int,
    pub fn update(self: *@This(), dims: @Vector(2, c_int)) void {
        self.dims = dims;
        self.tile_size = @divTrunc(9 * dims[1], 200);
        self.half_tile = @divTrunc(self.tile_size, 2);
        self.start = .{@divTrunc(dims[0] - 10 * self.tile_size, 2), @divTrunc(dims[1], BOARD_SZ.rows)};
        self.board_span = .{self.tile_size * BOARD_SZ.cols, self.tile_size * BOARD_SZ.rows};
        self.end = self.start + self.board_span;
    }
};

const BOARD_SZ = .{ .rows = 20, .cols = 10 };
const PREVIEW = 5;
const BAG = std.meta.tags(Mino).len;

var board: [BOARD_SZ.rows][BOARD_SZ.cols]Tile = undefined;
var current_piece: Piece = undefined;
var queue: MinoQueue(PREVIEW + BAG) = undefined;
var hold: ?Mino = null;
var swaped: bool = false;
var das_state: DasState = undefined;
var down_state: struct { time: f32, keypress: bool } = .{ .time = 0, .keypress = false };
var game_state: enum { running, game_over } = .running;
var settings: user_conf.Settings = undefined;
var screen_coords: ScreenCoords = undefined;

var rnd: std.rand.Xoshiro256 = undefined;
var rng: std.rand.Random = undefined;

pub fn main() !void {
    settings = try user_conf.loadConf();
    rnd = std.rand.DefaultPrng.init(@bitCast(std.time.microTimestamp()));
    rng = rnd.random();
    @memset(&board, .{.empty} ** 10);
    current_piece.spawn(false);

    if (settings.resolution) |res| {
        screen_coords.update(res);
        c.InitWindow(res[0], res[1], "Zetris");
    } else {
        c.InitWindow(0, 0, "Zetris");
        screen_coords.update(.{c.GetScreenWidth(), c.GetScreenHeight()});
        c.SetWindowState(c.FLAG_FULLSCREEN_MODE);
    }
    c.SetTargetFPS(60);
    c.SetExitKey(settings.key_binds.quit);

    while (!c.WindowShouldClose()) {
        c.BeginDrawing();
        c.ClearBackground(c.LIGHTGRAY);
        switch (game_state) {
            .running => {
                const frame_time = c.GetFrameTime();
                current_piece.updateLockT(frame_time);
                if (c.IsKeyPressed(settings.key_binds.hold) and !swaped)
                    current_piece.spawn(true);
                if (c.IsKeyPressed(settings.key_binds.drops[0]))
                    current_piece.drop();
                _ = down(frame_time, c.IsKeyDown(settings.key_binds.drops[1]));
                das_state.input((@as(u2, @intFromBool(c.IsKeyDown(settings.key_binds.directions[0]))) << 1) |
                    @intFromBool(c.IsKeyDown(settings.key_binds.directions[1])), frame_time);
                switch ((@as(u2, @intFromBool(c.IsKeyPressed(settings.key_binds.rotations[0]))) << 1) |
                    @intFromBool(c.IsKeyPressed(settings.key_binds.rotations[1]))) {
                    0b01 => _ = current_piece.rotate(.e),
                    0b10 => _ = current_piece.rotate(.w),
                    else => {},
                }
                drawBoard(true);
            },
            .game_over => {
                drawBoard(false);
                c.DrawText("Game Over", screen_coords.start[0] + screen_coords.half_tile, screen_coords.start[1] + screen_coords.half_tile, screen_coords.tile_size, c.WHITE);
            }
        }
        c.EndDrawing();
    }
    c.CloseWindow();
}

fn down(frame_time: f32, keypress: bool) bool {
    const KEYDELAY = 0.033;
    const AUTODELAY = 1.0;
    const delay: f32 = if (keypress) KEYDELAY else AUTODELAY;
    down_state.time += frame_time;
    if (!down_state.keypress and keypress)
        down_state.time *= KEYDELAY / AUTODELAY;
    down_state.keypress = keypress;
    const move = down_state.time >= delay;
    if (move) {
        _ = current_piece.move(.s);
        down_state.time -= delay;
    }
    return move;
}

fn drawBoard(comptime draw_piece: bool) void {
    if (config.debug_vis) {
        const debug_y = @divTrunc(screen_coords.dims[1], 2);
        c.DrawText("Current rotation:", screen_coords.half_tile, debug_y, screen_coords.half_tile, c.DARKGRAY);
        c.DrawTextCodepoint(c.GetFontDefault(), switch(current_piece.rotation)
            { .n => 'n', .e => 'e', .s => 's', .w => 'w', },
            .{ .x = @floatFromInt(screen_coords.half_tile), .y = @floatFromInt(debug_y + screen_coords.half_tile) }, @floatFromInt(screen_coords.half_tile), c.DARKGRAY);
    }
    c.DrawRectangle(screen_coords.start[0], screen_coords.start[1], screen_coords.board_span[0], screen_coords.board_span[1], c.DARKGRAY);
    for (board, 0..) |row, uy| {
        const y: c_int = @intCast(uy);
        for (row, 0..) |tile, ux| {
            const x: c_int = @intCast(ux);
            if (tile != .empty)
                c.DrawRectangle(screen_coords.tile_size * x + screen_coords.start[0], @intCast(screen_coords.end[1] - screen_coords.tile_size * (y + 1)), screen_coords.tile_size, screen_coords.tile_size, tile.color(false));
        }
    }
    if (draw_piece) current_piece.draw();
    for (1..BOARD_SZ.cols) |x| {
        const screen_x = screen_coords.tile_size * @as(c_int, @intCast(x)) + screen_coords.start[0];
        c.DrawLine(screen_x, screen_coords.start[1], screen_x, screen_coords.end[1], c.LIGHTGRAY);
    }
    for (1..BOARD_SZ.rows) |y| {
        const screen_y = screen_coords.tile_size * @as(c_int, @intCast(y)) + screen_coords.start[1];
        c.DrawLine(screen_coords.start[0], screen_y, screen_coords.end[0], screen_y, c.LIGHTGRAY);
    }

    const hold_start = screen_coords.start[0] - 9 * screen_coords.half_tile;
    c.DrawText("Hold", hold_start, screen_coords.start[1] - screen_coords.half_tile, screen_coords.half_tile, c.DARKGRAY);
    if (hold) |mino|
        mino.drawPreview(hold_start, screen_coords.start[1]);

    const queue_start = screen_coords.end[0] + screen_coords.half_tile;
    c.DrawText("Next", queue_start, screen_coords.start[1] - screen_coords.half_tile, screen_coords.half_tile, c.DARKGRAY);
    for (0..PREVIEW) |i| {
        const queued = queue.getUnchecked(@truncate(i));
        queued.drawPreview(queue_start, screen_coords.start[1] + 3 * screen_coords.tile_size * @as(c_int, @intCast(i)));
    }
}
